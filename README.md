# Madrid Shops

## What's this?

This is the solution to the Advanced iOS development module for KeepCoding's Bootcamp.

## How do I run this

1. clone
1. no need to run `pod install` (all pods committed to this repo, but it won't hurt)
1. uses Xcode 9.2 or newer / Swift 4


## Project structure

- external dependencies managed using __CocoaPods__. Refer to `Podfile` to see libraries used.
- There's only one project, `MadidShops`, but with several different targets:
- `Domain`: this target contains all domain related code (model, Interactors, cache, CoreData, etc.). This target also contain few Unit Tests (see `DomainTests`). This target could be moved into its own project, to have this common code shared between several different projects.
- `MadridShops`: target for the main App, containing UI code.

## SwiftLint

I've added SwiftLint to flag for suspicious or just plain bad code. A linter is [defined](https://en.wikipedia.org/wiki/Lint_(software)) as "...any tool that detects and flags errors in programming languages, including stylistic errors".

To install this in your own project:
- Add SwiftLint pod to your Podfile
- run `pod install`
- add a `Run Script` phase to your target. To do this:
    - go to Target > Build Phases > Press the "+" button
    - add a `run script` phase
    - paste the script inside

Now, with every build you'll see more warnings / errors. You can change SwiftLint config by editing the hidden file `.swiftlint.yml`. All rules are [here](https://github.com/realm/SwiftLint/blob/master/Rules.md)

## Server used to try this

https://fakemadrid.herokuapp.com/shops
https://fakemadrid.herokuapp.com/activities
