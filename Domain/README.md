#  Domain Framework

In this Framework we encapsulate all non-UI code. This way we can reuse this in a macOS app, for instance.

## Layers

- Network layer: all information that comes from the network is converted from String into JSON, then parsed into `NetworkEntity` objects. Found in `Network Entitites` folder
- Cache layer: we store everything in Core Data. In this case the cache acts also as our model. That is, we don't have a separate layer for objects in memory and objects in Core Data. Found in `Model` folder
- all Interactors (use cases) are inside the `Interactors` folder

