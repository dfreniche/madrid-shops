//
//  GetAllDataInteractor.swift
//  Domain
//
//  Created by Diego Freniche Brito on 20/12/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public protocol GetAllDataInteractor {
    // execute: downloads and caches all shops and activities model objects. Call onSuccess on the main thread
    func execute(onSuccess: @escaping () -> Void, onError: ErrorClosure)
}
