//
//  GetAllDataInteractorImpl.swift
//  Domain
//
//  Created by Diego Freniche Brito on 20/12/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation
import CoreData

public class GetAllDataInteractorImpl: GetAllDataInteractor {
    public var context: NSManagedObjectContext

    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    public func execute(onSuccess: @escaping () -> Void, onError: ErrorClosure) {
        self.downloadAndCacheShops(onDownloadAndCacheSuccess: onSuccess, onDownloadAndCacheError: onError)
    }
    
    func downloadAndCacheShops(onDownloadAndCacheSuccess: @escaping () -> Void, onDownloadAndCacheError: ErrorClosure) {
        let downloadShopsInteractor: DownloadAllEntitiesInteractor = DownloadAllEntitiesInteractorNSURLSessionImpl(url: "https://madrid-shops.com/json_new/getShops.php")
        
        downloadShopsInteractor.execute(onSuccess: { (shops) in
            let saveallShopsInteractor = SaveAllEntitiesInteractorImpl(entityType: Models.shopCD)
            saveallShopsInteractor.execute(entities: shops, context: self.context, onSuccess: { (shops: NetworkEntities<NetworkEntity>) in
                
                self.downloadAndCacheActivities(onDownloadAndCacheSuccess: onDownloadAndCacheSuccess, onDownloadAndCacheError: onDownloadAndCacheError)
            })
        }) { (error: Error) in
            onDownloadAndCacheError?(error)
        }
    }
    
    func downloadAndCacheActivities(onDownloadAndCacheSuccess: @escaping () -> Void, onDownloadAndCacheError: ErrorClosure) {
        let downloadActivitiesInteractor: DownloadAllEntitiesInteractor = DownloadAllEntitiesInteractorNSURLSessionImpl(url: "https://madrid-shops.com/json_new/getActivities.php")
        
        downloadActivitiesInteractor.execute { (activities: NetworkEntities<NetworkEntity>) in
            let saveAllActivitiesInteractor = SaveAllEntitiesInteractorImpl(entityType: Models.activityCD)
            saveAllActivitiesInteractor.execute(entities: activities, context: self.context, onSuccess: { (activities: NetworkEntities<NetworkEntity>) in

                // we've finished with Shops and Activities, set flag to never download again
                SetExecutedOnceInteractorImpl().execute()
                onDownloadAndCacheSuccess()
            }){ (error: Error) in
                onDownloadAndCacheError?(error)
            }
        }
    }
    
}
