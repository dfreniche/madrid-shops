//
//  SetExecutedOnceInteractorImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 18/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public class SetExecutedOnceInteractorImpl: SetExecutedOnceInteractor {
    let onceKey = "once"
    
    public init() {
        
    }

    public func execute() {
        execute(executedOnce: true)
    }
    
    public func execute(executedOnce: Bool) {
        let defaults = UserDefaults.standard
        
        if executedOnce {
            defaults.set("SAVED", forKey: onceKey)
        } else {
            defaults.removeObject(forKey: onceKey)
        }
        defaults.synchronize()
    }
}
