//
//  ReachabilityInteractorImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 30/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation
import SystemConfiguration

public class ReachabilityInteractorImpl: ReachabilityInteractor {
    var keepNotifying: Bool = true
    
    public init() {
        
    }
    
    public func execute(onConnectedToInternet: @escaping () -> Void, onDisconnectedFromInternet: @escaping () -> Void) {
        if let reachability = Reachability.init(hostname: "https://www.google.es") {
            reachability.whenReachable = {(reach: Reachability) -> Void in
                reach.stopNotifier()
                if (self.keepNotifying) {
                    onConnectedToInternet()
                }
                self.keepNotifying = false
            }
            
            reachability.whenUnreachable = {(reach: Reachability) -> Void in
                reach.stopNotifier()
                if (self.keepNotifying) {
                    onDisconnectedFromInternet()
                }
                self.keepNotifying = false
            }
            
            do {
                try reachability.startNotifier()
            } catch {
                
            }
        }
    }
}
