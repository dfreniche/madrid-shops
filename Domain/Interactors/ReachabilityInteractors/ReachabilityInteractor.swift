//
//  ReachabilityInteractor.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 30/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public protocol ReachabilityInteractor {
    func execute(onConnectedToInternet: @escaping () -> Void, onDisconnectedFromInternet: @escaping () -> Void)
}
