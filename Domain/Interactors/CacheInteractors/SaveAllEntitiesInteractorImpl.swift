//
//  SaveAllShopsInteractorImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 15/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import CoreData

// This interactor caches all network entities, no matter the type, into Core Data
public class SaveAllEntitiesInteractorImpl: SaveAllEntitiesInteractor {
    let entityType: Models
    
    public init(entityType: Models) {
        self.entityType = entityType
    }
    
    public func execute(entities: NetworkEntities<NetworkEntity>, context: NSManagedObjectContext, onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure) {
        
        for i in 0 ..< entities.count() {
            let entity = entities.get(index: i)
            
            switch (self.entityType) {
            case .shopCD:
                let _ = mapNetworkEntityIntoShopCD(context: context, shop: entity)
            case .activityCD:
                let _ = mapNetworkEntityIntoActivityCD(context: context, activity: entity)
            }
        }
        
        do {
            try context.save()
            onSuccess(entities)
        } catch {
            if let onError = onError {
                onError(error)
            }
        }
        
    }
    
    public func execute(entities: NetworkEntities<NetworkEntity>, context: NSManagedObjectContext, onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void) {
        execute(entities: entities, context: context, onSuccess: onSuccess, onError: nil)
    }
}
