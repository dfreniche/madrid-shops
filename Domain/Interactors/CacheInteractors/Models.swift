//
//  Models.swift
//  Domain
//
//  Created by Diego Freniche Brito on 01/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public enum Models {
    case shopCD
    case activityCD
}
