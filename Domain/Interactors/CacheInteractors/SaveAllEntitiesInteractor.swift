//
//  SaveAllShopsinteractor.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 15/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import CoreData

public protocol SaveAllEntitiesInteractor {
    // execute: saves all shops. Return on the main thread
    func execute(entities: NetworkEntities<NetworkEntity>, context: NSManagedObjectContext, onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure)
    func execute(entities: NetworkEntities<NetworkEntity>, context: NSManagedObjectContext, onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void)
}
