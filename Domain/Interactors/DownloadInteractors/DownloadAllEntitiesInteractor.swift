//
//  DownloadAllShopsInteractor.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public protocol DownloadAllEntitiesInteractor {
    // execute: downloads all entities, shops or activities. Return on the main thread
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure)
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void)
}
