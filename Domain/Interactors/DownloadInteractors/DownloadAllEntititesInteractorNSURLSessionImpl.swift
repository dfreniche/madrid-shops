//
//  DownloadAllShopsInteractorNSURLSessionImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 12/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public class DownloadAllEntitiesInteractorNSURLSessionImpl: DownloadAllEntitiesInteractor {
    
    let urlString: String!
    
    public init(url: String) {
        self.urlString = url
    }
    
    public func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure) {
        // let urlString = "https://madrid-shops.com/json_new/getShops.php"
        
        let session = URLSession.shared
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 5)
            let task = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in

                OperationQueue.main.addOperation {
                    assert(Thread.current == Thread.main)

                    if error == nil {
                        // OK
                        
                        let shops = parseShops(data: data!)
                        onSuccess(shops)
                    } else {
                        // Error
                        if let myError = onError {
                            myError(error!)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    public func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void) {
        execute(onSuccess: onSuccess, onError: nil)
    }
    
    
}
