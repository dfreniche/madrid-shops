//
//  DownloadAllShopsInteractorFakeImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

class DownloadAllEntitiesInteractorFakeImpl: DownloadAllEntitiesInteractor {
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void) {
        execute(onSuccess: onSuccess, onError: nil)
    }
    
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure = nil) {
        let shops = NetworkEntities<NetworkEntity>()
        
        for i in 0...10 {
            let shop = NetworkEntity(name: "Shop number \( i )")
            shop.address = "Address \( 1 )"
            
            shops.add(shop: shop)
        }
        
        OperationQueue.main.addOperation {
           onSuccess(shops)
        }
    }
    
    
}
