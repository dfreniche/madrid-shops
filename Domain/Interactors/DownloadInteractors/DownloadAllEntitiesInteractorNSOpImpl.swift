//
//  DownloadAllShopsInteractorNSOpImpl.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 11/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

class DownloadAllEntitiesInteractorNSOpImpl: DownloadAllEntitiesInteractor {
    let urlString: String!
    
    public init(url: String) {
        self.urlString = url
    }
    
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void) {
        execute(onSuccess: onSuccess, onError: nil)
    }
    
    func execute(onSuccess: @escaping (NetworkEntities<NetworkEntity>) -> Void, onError: ErrorClosure = nil) {

        let queue = OperationQueue()
        queue.addOperation {
            if let url = URL(string: self.urlString), let data = NSData(contentsOf: url) as Data? {
                let shops = parseShops(data: data)
                    
                OperationQueue.main.addOperation {
                    onSuccess(shops)
                }
            }
        }
    }
}
