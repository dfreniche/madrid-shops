//
//  Typealias.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

// closure passed to different Interactors to be notified of errors
public typealias ErrorClosure = ((Error) -> Void)?
