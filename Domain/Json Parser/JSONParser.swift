//
//  JSONParser.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 12/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

func parseShops(data: Data) -> NetworkEntities<NetworkEntity> {
    let shops = NetworkEntities<NetworkEntity>()
    do {
        let jsonObject = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, Any>
        let result = jsonObject["result"] as! [Dictionary<String, Any>]
        
        for shopJson in result {
            let shop = NetworkEntity(name: shopJson["name"]! as! String)
            shop.address = (shopJson["address"]! as? String) ?? "Default address"
            shop.logo = (shopJson["logo_img"] as? String) ?? ""
            shop.image = (shopJson["img"] as? String) ?? ""
            shop.description = shopJson["description_en"] as! String
            if let latitudeJson = shopJson["gps_lat"] as? NSString {
                shop.latitude = latitudeJson.floatValue
            }
            if let longitudeJson = shopJson["gps_lon"] as? NSString {
                shop.longitude = longitudeJson.floatValue
            }
            shops.add(shop: shop)
        }
    } catch {
        print("Error parsing JSON")
    }
    return shops
}
