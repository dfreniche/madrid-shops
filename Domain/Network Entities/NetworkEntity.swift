//
//  Shop.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

// as all entities in our services are quite similar I'm going to use this model class
// to encapsulate all data that come from JSON requests
// a NetworkEntity will have more fields than needed in our app
// we can also use this as a base class and extend as needed

public class NetworkEntity {
    public var name: String
    public var description: String = ""
    public var latitude: Float? = nil
    public var longitude: Float? = nil
    public var image: String = ""
    public var logo: String = ""
    public var openingHours: String = ""
    public var address: String = ""
    
    public init(name: String) {
        self.name = name
    }
}
