//
//  AggregateProtocol.swift
//  Domain
//
//  Created by Diego Freniche Brito on 01/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

/**
 * represents an aggregate of objects, with a natural order based on index
 */
public protocol AggregateProtocol {
    associatedtype T
    
    func count() -> Int
    func add(shop: T)
    func get(index: Int) -> T
}
