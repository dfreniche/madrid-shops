//
//  Shops.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public class NetworkEntities<T>: AggregateProtocol {
    private var shopsList: [T]?

    public init() {
        self.shopsList = []
    }
    
    public func count() -> Int {
        return (shopsList?.count)!
    }
    
    public func add(shop: T) {
        shopsList?.append(shop)
    }
    
    public func get(index: Int) -> T {
        return (shopsList?[index])!
    }
}
