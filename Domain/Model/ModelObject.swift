//
//  ModelObject.swift
//  Domain
//
//  Created by Diego Freniche Brito on 20/12/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public protocol ModelObject {
    var name: String? {get set}
    var address: String? {get set}
    var latitude: Float {get set}
    var longitude: Float {get set}
}
