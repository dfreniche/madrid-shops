//
//  ConfigureCache.swift
//  Domain
//
//  Created by Diego Freniche Brito on 03/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

public func configureCache() {
    let cacheSize = 250 * 1024 * 1024
    let cacheDiskSize = 250 * 1024 * 1024;
    let imageCache = URLCache(memoryCapacity: cacheSize, diskCapacity: cacheDiskSize, diskPath: Bundle.main.bundlePath + "/ImageCacheFile")

    URLCache.shared = imageCache
}
