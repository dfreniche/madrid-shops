//
//  CoreDataStack.swift
//  CoreDataHelloWorld
//
//  Created by Diego Freniche Brito on 13/09/2017.
//  Copyright © 2017 Diego Freniche Brito. All rights reserved.
//

import CoreData

public class CoreDataStack {
    public init() {
        
    }
    
    public func createContainer(dbName: String) -> NSPersistentContainer {
        let container = NSPersistentContainer(name: dbName, bundle: Bundle(for: CoreDataStack.self))
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            print("💾 \( storeDescription.description )")
            //  ¯\_(ツ)_/¯
            if let error = error as NSError? {
                fatalError("💩 Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }
    
    public func saveContext(context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// Frameworks reside in ots own folder (Bundle). So we use this extension to load
// model properly

extension NSPersistentContainer {
    public convenience init(name: String, bundle: Bundle) {
        guard let modelURL = bundle.url(forResource: name, withExtension: "momd"),
            let mom = NSManagedObjectModel(contentsOf: modelURL)
            else {
                fatalError("Unable to located Core Data model")
        }
        
        self.init(name: name, managedObjectModel: mom)
    }
    
}
