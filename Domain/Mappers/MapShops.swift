//
//  MapShops.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 18/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import CoreData

public func mapShopCDIntoNetworkEntity(shopCD: ShopCD) -> NetworkEntity {
    let shop = NetworkEntity(name: shopCD.name ?? "Empty")
    shop.address = shopCD.address ?? ""
    shop.image = shopCD.image ?? ""
    shop.logo = shopCD.logo ?? ""
    
    shop.latitude = shopCD.latitude
    shop.longitude = shopCD.longitude
    
    shop.description = shopCD.description_en ?? ""
    shop.openingHours = shopCD.openingHours ?? ""
    
    return shop
}

public func mapNetworkEntityIntoShopCD(context: NSManagedObjectContext, shop: NetworkEntity) -> ShopCD {
    // mapping shop into ShopCD
    let shopCD = ShopCD(context: context)
    shopCD.name = shop.name
    shopCD.address = shop.address
    shopCD.image = shop.image
    shopCD.logo = shop.logo
    
    shopCD.latitude = shop.latitude ?? 0.0
    shopCD.longitude = shop.longitude ?? 0.0
    shopCD.description_en = shop.description
    shopCD.openingHours = shop.openingHours
    
    return shopCD
}

public func mapNetworkEntityIntoActivityCD(context: NSManagedObjectContext, activity: NetworkEntity) -> ActivityCD {
    // mapping shop into ShopCD
    let shopCD = ActivityCD(context: context)
    shopCD.name = activity.name
    shopCD.address = activity.address
    shopCD.image = activity.image
    shopCD.logo = activity.logo
    
    shopCD.latitude = activity.latitude ?? 0.0
    shopCD.longitude = activity.longitude ?? 0.0
    shopCD.description_en = activity.description
    shopCD.openingHours = activity.openingHours
    
    return shopCD
}
