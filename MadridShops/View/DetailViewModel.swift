//
//  DetiViewModel.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 03/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation
import Domain
import CoreData

// ViewModel used to "paint" data in DetailViewController

public class DetailViewModel {
    public let name: String
    public let description: String?
    public let image: String?
    
    public init(element: NSManagedObject, type: Models) {
        switch type {
        case .shopCD:
            self.name = (element as! ShopCD).name ?? "No name"
            self.description = (element as! ShopCD).description_en ?? ""
            self.image = (element as! ShopCD).image ?? ""
        case .activityCD:
            self.name = (element as! ActivityCD).name ?? "No name"
            self.description = (element as! ActivityCD).description_en ?? ""
            self.image = (element as! ActivityCD).image ?? ""
        }
    }
}
