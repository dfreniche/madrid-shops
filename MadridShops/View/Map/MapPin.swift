//
//  MapPin.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 18/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import MapKit
import Domain

class MapPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var data: ModelObject
    var dataType: Models
    
    init(coordinate: CLLocationCoordinate2D, data: ModelObject, dataType: Models) {
        self.coordinate = coordinate
        self.data = data
        self.dataType = dataType
    }
}
