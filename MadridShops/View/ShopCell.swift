//
//  ShopCell.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 08/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit
import Domain

class ShopCell: UICollectionViewCell {
    var modelObject: CellViewModel?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!

    func refresh(shop: CellViewModel) {
        self.modelObject = shop
        
        self.label.text = shop.name
        self.modelObject?.logo?.loadImage(into: imageView)
        imageView.clipsToBounds = true
        UIView.animate(withDuration: 1.0) {
            self.imageView.layer.cornerRadius = 30
        }
    }
}
