//
//  ViewModel.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 01/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation
import Domain
import CoreData

// ViewModel used to "paint" data in Cells
// this just encapsulates all needed data for painting

public class CellViewModel {
    public let name: String
    public let logo: String?
    
    public init(element: NSManagedObject, type: Models) {
        switch type {
        case .shopCD:
            self.name = (element as! ShopCD).name ?? "No name"
            self.logo = (element as! ShopCD).logo ?? ""
        case .activityCD:
            self.name = (element as! ActivityCD).name ?? "No name"
            self.logo = (element as! ActivityCD).logo ?? ""
            
        }
    }
}
