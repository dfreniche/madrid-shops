//
//  AppDelegate.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit
import CoreData
import Domain

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var cds = CoreDataStack()
    var context: NSManagedObjectContext?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // init the NSURLSession cache
        configureCache()
        
        // init Core Data and get a context to inject
        self.context = cds.createContainer(dbName: "MadridShops").viewContext
        
        // get a pointer to the 1st ViewController
        let nav = self.window?.rootViewController as! UINavigationController
        let mainVC = nav.topViewController as! MainMenuViewController
        
        // inject the context
        mainVC.context = self.context
        
        return true
    }


    func applicationDidEnterBackground(_ application: UIApplication) {
        // when we enter background we save all pending changes in Core Data
        guard let context = self.context else { return }
        self.cds.saveContext(context: context)
    }
}

