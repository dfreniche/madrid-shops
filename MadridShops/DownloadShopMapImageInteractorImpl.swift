//
//  DownloadShopMapImageInteractorImpl.swift
//  Domain
//
//  Created by Diego Freniche Brito on 05/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

class DownloadShopMapImageInteractorImpl {
    func execute(shop: ShopCD, onSuccess: @escaping (UIImage) -> Void, onError: ErrorClosure) {
        
        let latitude = shop.latitude
        let longitude = shop.longitude
        let session = URLSession.shared
        let URLString = "http://maps.googleapis.com/maps/api/staticmap? center=\(latitude),\(longitude)&zoom=17&size=320x220&scale =2&markers=%7Ccolor:0x9C7B14%7C\(latitude),\(longitude)"
        
        if let url = URL(string: URLString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) {
            let task = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if error == nil {
                    // OK
                    if let image = UIImage(data: data!) {
                        onSuccess(image)
                    }
                } else {
                    if let onError = onError {
                        print("💩")
                        onError(error!)
                    }
                }
            }
            task.resume()
        }
    }
    
    func execute(shop: ShopCD, onSuccess: @escaping (UIImage) -> Void) {
        execute(shop: shop, onSuccess: onSuccess, onError: nil)
    }
    
    
}
