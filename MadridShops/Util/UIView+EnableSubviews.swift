//
//  UIView+EnableSubviews.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 30/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit

extension UIView {
    func enableAllSubviews(enabled: Bool) {
        // using a map here instead of a for, just for fun
        // this is a bad practice, it's better to use a for each
        // because we're using a map just to create a side effect
        // which is HORRIBLE and anti - functional programming
        let _ = self.subviews.map { (u: UIView) -> Void in
            if let control = u as? UIControl {
                control.isEnabled = enabled
            }
        }
    }
}
