//
//  String+DebugPrint.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 30/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import Foundation

// an extension to print debug info only when we're in a debug build

extension String {
    func debug🛠() {
#if DEBUG
        print(self)
#endif
    }
}
