//
//  ListViewController+Map.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 18/10/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import MapKit
import Domain

extension ListViewController: MKMapViewDelegate {
    
    func centerMap() {
        let madridLocation = CLLocation(latitude:40.41889 , longitude: -3.69194)
        self.map.setCenter(madridLocation.coordinate, animated: true)
    }
    
    func addPins(points: [ModelObject], map: MKMapView) {
        for point in points {
            var lat = 0.0
            var lon = 0.0
            var dataType = Models.shopCD
            if let shop = point as? ShopCD {
                lat = CLLocationDegrees(shop.latitude)
                lon = CLLocationDegrees(shop.longitude)
            } else if let activity = point as? ActivityCD {
                lat = CLLocationDegrees(activity.latitude)
                lon = CLLocationDegrees(activity.longitude)
                dataType = .activityCD
            }
            
            let a1 = MapPin(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lon), data: point, dataType: dataType)
            a1.title = point.name
            a1.subtitle = point.address
            
            map.addAnnotation(a1)
        }
    }
    
    // MARK: - MKMapViewDelegate delegate methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this a class property?
        let annotationIdentifier = "AnnotationIdentifier"
        var pinView: MKPinAnnotationView? = nil

        if let annotationPin = annotation as? MapPin {
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKPinAnnotationView {
                pinView = dequeuedAnnotationView
                pinView?.annotation = annotationPin
            }
            else {
                pinView = MKPinAnnotationView(annotation: annotationPin, reuseIdentifier: annotationIdentifier)
                pinView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                pinView?.canShowCallout = true
            }
        }
        
        if let pinView = pinView, let pin = annotation as? MapPin {
            configureAnnotationView(annotation: pin, view: pinView)
            pinView.canShowCallout = true
        }
        
        return pinView
    }
    
    func configureAnnotationView(annotation: MapPin, view: MKPinAnnotationView) {
        switch annotation.dataType {
        case .shopCD:
            view.pinTintColor = MKPinAnnotationView.redPinColor()
        case .activityCD:
            view.pinTintColor = MKPinAnnotationView.greenPinColor()
        }
        
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    
    // Tells the delegate that the map view is about to start rendering some of its tiles
    func mapViewWillStartRenderingMap(_ mapView: MKMapView) {
        print("Start rendering")
    }
    
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        print("Finish rendering")
        if (!self.alreadyAddedPinsToMap) {
            self.addPins(points: self.fetchedResultsController.fetchedObjects as! [ModelObject], map: self.map)
            self.alreadyAddedPinsToMap = true
        }
    }
    
    // Tells the delegate that the specified map view is about to retrieve some map data
    func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        print("Start loading")
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        print("Finish loading")
    }

}
