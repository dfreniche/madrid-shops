//
//  ViewController.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 07/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit
import Domain

class ListViewController: UIViewController, CLLocationManagerDelegate {

    // manually injected vars
    var modelType: Models!
    var context: NSManagedObjectContext!
    
    // Iboutlets
    @IBOutlet weak var shopsCollectionView: UICollectionView!
    @IBOutlet weak var map: MKMapView!
    
    var alreadyAddedPinsToMap: Bool = false
    
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
                
        self.shopsCollectionView.delegate = self
        self.shopsCollectionView.dataSource = self
        
        self.centerMap()
        
        self.map.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var element: NSManagedObject? = nil
        let segueName: String
        switch self.modelType! {
        case .shopCD:
            element = self.fetchedResultsController.object(at: indexPath) as! ShopCD
            segueName = "ShowShopDetailSegue"
        case .activityCD:
            element = self.fetchedResultsController.object(at: indexPath) as! ActivityCD
            segueName = "ShowActivityDetailSegue"
        }
        
        self.performSegue(withIdentifier: segueName, sender: element!)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowShopDetailSegue" {
            let vc = segue.destination as! ShopDetailViewController

            let shopCD: ShopCD = sender as! ShopCD
            vc.shop = shopCD
        }
        
        if segue.identifier == "ShowActivityDetailSegue" {
            let vc = segue.destination as! ShopDetailViewController
            
            let shopCD: ActivityCD = sender as! ActivityCD
            vc.activity = shopCD
        }
    }

    // MARK: - Fetched results controller
    var _fetchedResultsController: NSFetchedResultsController<NSManagedObject>? = nil

    var fetchedResultsController: NSFetchedResultsController<NSManagedObject> {
        if (_fetchedResultsController != nil) {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult>!
        let cacheFileName: String!
        
        switch self.modelType! {
        case .shopCD:
            fetchRequest = ShopCD.fetchRequest()
            cacheFileName = "shopsCache"
        case .activityCD:
            fetchRequest = ActivityCD.fetchRequest()
            cacheFileName = "activitiesCache"
        }
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        // fetchRequest == SELECT * FROM EVENT ORDER BY TIMESTAMP DESC
        _fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.context!, sectionNameKeyPath: nil, cacheName: cacheFileName) as? NSFetchedResultsController<NSManagedObject>
        // aFetchedResultsController.delegate = self
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        //self.map.setCenter(location.coordinate, animated: true)
    }
    

}

