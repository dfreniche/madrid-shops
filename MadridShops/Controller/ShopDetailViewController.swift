//
//  ShopDetailViewController.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 12/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit
import Domain

class ShopDetailViewController: UIViewController {

    var shop: ShopCD!
    var activity: ActivityCD!
    
    @IBOutlet weak var shopDetailDescription: UITextView!
    
    @IBOutlet weak var shopImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (self.shop != nil) {
            self.title = self.shop.name
            self.shopDetailDescription.text = self.shop.description_en
            self.shop.image?.loadImage(into: shopImage)
        } else {
            self.title = self.activity.name
            self.shopDetailDescription.text = self.activity.description_en
            self.activity.image?.loadImage(into: shopImage)
        }
    }

    

}
