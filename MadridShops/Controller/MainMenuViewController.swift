//
//  MainMenuViewController.swift
//  MadridShops
//
//  Created by Diego Freniche Brito on 27/09/2017.
//  Copyright © 2017 KC. All rights reserved.
//

import UIKit
import CoreData
import Domain

class MainMenuViewController: UIViewController {
    // this context needs to be injected
    public var context: NSManagedObjectContext!

    // outlets
    @IBOutlet weak var shopsButton: UIButton!
    @IBOutlet weak var activitiesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeData()
    }

    func initializeData() {
        // if we have Internet connection...
        let reachableTest = ReachabilityInteractorImpl()
        reachableTest.execute(onConnectedToInternet: {
            "Connected".debug🛠()

            // we execute once...
            ExecuteOnceInteractorImpl().execute {
                // disabling all buttons in this view
                self.view.enableAllSubviews(enabled: false)
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                self.downloadAndCacheData()
            }
        }) {    // not connected to Internet
            "Not connected".debug🛠()
            self.view.enableAllSubviews(enabled: true)
            self.showError()
        }
    }
    
    func downloadAndCacheData() {
        let downloadInteractor: GetAllDataInteractor = GetAllDataInteractorImpl(context: self.context)
            self.view.enableAllSubviews(enabled: true)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        downloadInteractor.execute(onSuccess: {
            
        }) { (error: Error) in
            "Error".debug🛠()
            self.showError()
        }
    }
    
    func showError() {
        let alert = UIAlertController(title: "Error", message: "No connection to Internet", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: "Default action"), style: .`default`, handler: { _ in
            self.initializeData()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowShopsSegue" {
            let vc = segue.destination as! ListViewController
            vc.context = self.context
            vc.modelType = .shopCD
        }
        
        if segue.identifier == "ShowActivitiesSegue" {
            let vc = segue.destination as! ListViewController
            vc.context = self.context
            vc.modelType = .activityCD
        }
    }

}
